    const express = require('express');
    const mongoose = require ('mongoose');

    const dotenv = require ('dotenv');
    const { request, response } = require('express');
// To initialize the dotenv in order to use for our  application.
    dotenv.config();

    const app = express();
    const port = 3001;

// MONGOOSE CONNECTION
// Mongoose uses the 'connect' function to connect to the cluster in our MongoDB Atlas.
/**
    It takes two arguments:
    1. connection string from Mongodb atlas
    2. Object that contains the middleware/standard that Mongodb uses 
 */
    mongoose.connect(`mongodb://127.0.0.1:27017/test`, 
        {
            useNewUrlParser : true,
            useUnifiedTopology : true
        }
    );
// Initialize the mongoose connection to the mongodb database by assigning "mongoose.connection"to the db variable
    let db = mongoose.connection

/*lister to the events of the connection by using the '.on()' function of the
mongoose mongoose.connection. and log  details in the console based on the event. (error or successful connection).
*/
    db.on('error', console.error.bind(console, "Connection Error"));
    db.on('open', () => console.log('Connection to MongoDB!'));

// CREATING A SCHEMA
    const taskSchema = new mongoose.Schema({
        name: String,
        status: {
            type: String,
            default: 'Pending'
        }
    });

// CREATING A MODEL
    const Task = mongoose.model('Task', taskSchema);

// CREATING ROUTES

// middleware
    app.use(express.json());
    app.use(express.urlencoded({extended: true}));

// Creating single task - route
    app.post('/tasks', (request, response) => {
/*
    Use the task model to find a similar entry in the database based on the
    user input from the req.body (or from postman).
*/
    Task.findOne({name: request.body.name}, (error, result) => {
/*
    Handle errors amd success by using the 'error' and 'result' var
    and check if one of them contains anything.*/
        if (error){
            return response.send(error)
        }
/*
    Check if the name from postman bbody has a similar entry in the database. if
    there is a similar entry, return a res saying the a dup task has been found.*/
            if(result != null && result.name == request.body.name) {

                return response.send('Duplicate task found!')
            } else {
    /*
    If there are no dup entries, then create a nre task out of the req.
    body and save it to the database.*/
                let newTask = new Task({
                    name: request.body.name
                });

                newTask.save((error, savedTask) => {
                    if(error){
                        return console.error(error)
                    }
                    return  response.status(201).send('New Task Created!')
                });
            };
        });
    });
    

    app.get('/tasks', (request, response) => {
        return Task.find({}, (error, result) => {
            if (error){
                response.send(error);
            }
            response.send(result);
        });
    });


// Creating users Collection

// SCHEMA
    const userSchema = new mongoose.Schema({
        username: String,
        password: String
    });

// MODEL
    const User = mongoose.model('User', userSchema);

// ROUTES
// Register single user - route
    app.post('/register', (request, response) => {
        User.findOne({username: request.body.username}, (error, result) => {

        // Check for duplicates
            if (result != null && result.username == request.body.username){
                return response.send('Duplicate user found!');

            // if no duplicates
            } else {
                // Check if the username and the password contains a string, if it is empty the code will go to the else statement.
                if (request.body.username !== '' && request.body.password !== '') {
                // If both username & password contains value, then continue with the process of saving that user as a new user in the database collection.
                    let newUser = new User({
                        username: request.body.username,
                        password: request.body.password
                    });

                    newUser.save((error, savedUser) => {
                        if (error){
                            return response.send(error);
                        } else {
                            return response.send("User registered successfully!");
                        }
                    });

                } else {
                        return response.send('BOTH Username AND Password must be provided.');
                    };
            };
        });
    });

// Get all users
    app.get('/users', (request, response) => {
        return User.find({}, (error, result) => {
            if (error){
                response.send(error);
            }
            response.send(result);
        });
    });


// app.listen should always be at the bottom of the server 
    app.listen(port, () => console.log(`server is running at port ${port}`));


